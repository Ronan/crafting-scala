package com.leroy.ronan.stack

import java.util.EmptyStackException

class Stack[A] {

  private var content: List[A] = Nil

  def pop(): A = {
    if (content == Nil) {
      throw new EmptyStackException
    }
    val result = content.head
    content = content.tail
    result
  }

  def push(value: A): Unit = {
    content = value :: content
  }
}
