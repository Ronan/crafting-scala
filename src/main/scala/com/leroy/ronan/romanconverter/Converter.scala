package com.leroy.ronan.romanconverter

object Converter {

  private val symbols =
    ("I",    1)::("IV",   4)::("V",   5)::("IX",   9)::
    ("X",   10)::("XL",  40)::("L",  50)::("XC",  90)::
    ("C",  100)::("CD", 400)::("D", 500)::("CM", 900)::
    ("M", 1000):: Nil

  def convert(input: Int): String = {
    var roman = ""
    var arabic = input

    for ((symbol, value) <- symbols reverse) {
      while (arabic >= value) {
        roman = roman + symbol
        arabic = arabic - value
      }
    }
    roman
 }
}

