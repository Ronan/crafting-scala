package com.leroy.ronan.leapyear

class Year(value: Int) {

  def isLeap : Boolean = {
    if (isDivisibleBy(400)) true
    else if (isDivisibleBy(100)) false
    else if (isDivisibleBy(4)) true
    else false
  }

  def isDivisibleBy(divisor : Int) : Boolean = {
    value % divisor == 0
  }

}
