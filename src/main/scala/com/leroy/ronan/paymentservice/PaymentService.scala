package com.leroy.ronan.paymentservice

class PaymentService (
                       sendToGateway : PaymentDetails => Unit,
                       isValidUser : User => Boolean
                     ) {

  def processPayment(user: User, paymentDetails: PaymentDetails): Unit = {
    if (!isValidUser(user))
      throw new InvalidUserException
    sendToGateway.apply(paymentDetails)
  }

}
