package com.leroy.ronan.tripservice.user

import com.leroy.ronan.tripservice.exception.CollaboratorCallException

object UserSession {

	def getLoggedUser(): User = {
		throw new CollaboratorCallException(
			"UserSession.getLoggedUser() should not be called in an unit test");
	}

}
