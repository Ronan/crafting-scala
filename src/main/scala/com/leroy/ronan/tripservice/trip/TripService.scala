package com.leroy.ronan.tripservice.trip

import com.leroy.ronan.tripservice.user.{UserSession, User}
import com.leroy.ronan.tripservice.exception.UserNotLoggedInException

class TripService (
										val getLoggedUser: () => User = UserSession.getLoggedUser,
										val findTripsByUser: User => List[Trip]= TripDAO.findTripsByUser
									) {

	def getTripsByUser(user: User): List[Trip] = {
		val loggedInUser: User = ensureUserIsLoggedIn
		if (isUserFriendOf(user, loggedInUser)) {
			return findTripsByUser(user)
		} else {
			return List()
		}
	}

	private def ensureUserIsLoggedIn = {
		val loggedInUser = getLoggedUser()
		if (loggedInUser == null) {
			throw new UserNotLoggedInException
		}
		loggedInUser
	}

	private def isUserFriendOf(user: User, other: User) = {
		user.friends().contains(other)
	}
}
