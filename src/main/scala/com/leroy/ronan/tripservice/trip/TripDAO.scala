package com.leroy.ronan.tripservice.trip

import com.leroy.ronan.tripservice.user.User
import com.leroy.ronan.tripservice.exception.CollaboratorCallException

object TripDAO {

	def findTripsByUser(user: User): List[Trip] = {
		throw new CollaboratorCallException(
			"TripDAO should not be invoked on an unit test.");
	}

}
