package com.leroy.ronan.tripservice

import com.leroy.ronan.tripservice.user.{UserSession, User}
import com.leroy.ronan.tripservice.trip.{TripDAO, Trip}
import scala.util.control.Breaks._
import com.leroy.ronan.tripservice.exception.UserNotLoggedInException

class Original_TripService {

	def getTripsByUser(user: User): List[Trip] = {
		var tripList: List[Trip] = List()
		val loggedInUser = UserSession getLoggedUser()
		var isFriend = false
		if (loggedInUser != null) {
			breakable { for (friend <- user.friends()) {
				if (friend == loggedInUser) {
					isFriend = true
					break
				}
			}}
			if (isFriend) {
				tripList = TripDAO.findTripsByUser(user)
			}
			tripList
		} else {
			throw new UserNotLoggedInException
		}
	}

}
