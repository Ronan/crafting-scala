package com.leroy.ronan.tripservice.exception

class CollaboratorCallException(message: String = null, cause: Throwable = null)
		extends RuntimeException(message, cause) {

}
