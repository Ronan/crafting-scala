package com.leroy.ronan.tripservice.trip

import com.leroy.ronan.tripservice.exception.UserNotLoggedInException
import com.leroy.ronan.tripservice.infrastructure.UnitSpec
import com.leroy.ronan.tripservice.user.User
import org.scalatest.BeforeAndAfter

class TripServiceSpec extends UnitSpec with BeforeAndAfter {

  val bob: User = new User
  val bill: User = new User

  before {
    bob.addTrip(new Trip())
    bob.addTrip(new Trip())
    bob.addTrip(new Trip())
    bill.addTrip(new Trip())
    bill.addTrip(new Trip())
  }

  "A trip service" should "raise UserNotLoggedInException when no user is logged in" in {
    val service = new TripService(
      () => null
    )
    assertThrows[UserNotLoggedInException] {
      service.getTripsByUser(null)
    }
  }

  "A trip service" should "retrieve no trips when bob is logged in and bill is not his friend" in {
    val service = new TripService(
      () => bob
    )

    val trips = service.getTripsByUser(bill)

    assertResult(List())(trips)
  }

  "A trip service" should "retrieve bill's trips when bob is logged in and bill is his friend" in {
    bill.addFriend(bob)
    val service = new TripService(
      () => bob,
      user => user.trips()
    )

    val trips = service.getTripsByUser(bill)

    assertResult(bill.trips())(trips)
  }

}
