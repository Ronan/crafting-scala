package com.leroy.ronan.romanconverter

import org.junit.Test
import org.scalatest.Assertions

class RomanConverterShould extends Assertions {

  @Test def convert_1_into_I(): Unit = {
    assertResult("I")(Converter convert 1)
  }

  @Test def convert_2_into_II(): Unit = {
    assertResult("II")(Converter convert 2)
  }

  @Test def convert_3_into_III(): Unit = {
    assertResult("III")(Converter convert 3)
  }

  @Test def convert_5_into_V(): Unit = {
    assertResult("V")(Converter convert 5)
  }

  @Test def convert_4_into_IV(): Unit = {
    assertResult("IV")(Converter convert 4)
  }

  @Test def convert_2499_into_MMCDXCIX(): Unit = {
    assertResult("MMCDXCIX")(Converter convert 2499)
  }

}
