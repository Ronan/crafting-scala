package com.leroy.ronan.leapyear

import org.junit.Assert.assertTrue
import org.junit.Test
import org.scalatest.Assertions

class YearShould extends Assertions {

  @Test def be_leap_when_it_is_2000(): Unit = {
    assertTrue(new Year(2000) isLeap)
  }

  @Test def not_be_leap_when_it_is_1900(): Unit = {
    assertTrue(!(new Year(1900) isLeap))
  }

  @Test def be_leap_when_it_is_2020(): Unit = {
    assertTrue(new Year(2020) isLeap)
  }

  @Test def not_be_leap_when_it_is_2019(): Unit = {
    assertTrue(!(new Year(2019) isLeap))
  }

}
