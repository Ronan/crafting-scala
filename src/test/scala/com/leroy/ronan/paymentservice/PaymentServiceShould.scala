package com.leroy.ronan.paymentservice

import org.junit.Assert.assertTrue
import org.junit.{Before, Test}
import org.scalatest.Assertions

class PaymentServiceShould extends Assertions {

  private val VALID_USER = new User{}
  private val INVALID_USER = new User{}
  def isValidUser(user: User): Boolean = {
    VALID_USER == user
  }

  private val DETAILS = new PaymentDetails {}
  private var gatewayQueue: List[PaymentDetails] = Nil
  def sendToGateway(details: PaymentDetails): Unit = {
    gatewayQueue = details::gatewayQueue
  }

  var service = new PaymentService(sendToGateway, isValidUser)

  @Before def setup(): Unit = {
    gatewayQueue = Nil
  }

  @Test def throws_exception_when_user_is_not_valid(): Unit = {
    assertThrows[InvalidUserException] {
      service processPayment(INVALID_USER, DETAILS)
    }
  }

  @Test def send_the_payment_to_the_gateway(): Unit = {
    service processPayment(VALID_USER, DETAILS)
    assertTrue(gatewayQueue.contains(DETAILS))
  }

}
