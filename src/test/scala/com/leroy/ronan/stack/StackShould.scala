package com.leroy.ronan.stack

import java.util.EmptyStackException

import org.junit.Test
import org.scalatest.Assertions

class StackShould extends Assertions {

  @Test def accept_a_push(): Unit = {
    val stack = new Stack[String]
    stack.push("foo")
  }

  @Test def raise_exception_on_popped_when_empty(): Unit = {
    val stack = new Stack[String]
    assertThrows[EmptyStackException] {
      stack pop()
    }
  }

  @Test def pop_previously_pushed_object(): Unit = {
    val stack = new Stack[String]
    stack push "foo"

    assertResult("foo")(stack pop())
  }

  @Test def pop_c_b_a_when_a_b_c_were_pushed(): Unit = {
    val stack = new Stack[String]
    stack push "a"
    stack push "b"
    stack push "c"

    assertResult("c")(stack pop())
    assertResult("b")(stack pop())
    assertResult("a")(stack pop())
  }
}